from Store import warehouse
from Store import good
import json


def OvalDanceWear():
    # creating warehouse
    store_warehouse = warehouse.Warehouse("Oval Dance Wear")
#
#     # getting goods data from text file
    with open('Selenium/data.json', encoding='utf-8') as f:
#     with open('../Selenium/data.json', encoding='utf-8') as f:
        l_dict = json.load(f)
#     # print(l_dict)


    # extending goods to warehouse
    instances = []
    for cat, good_info in l_dict.items():
        for name, price in good_info.items():
            instances.append(good.Good(name, price, cat))
            store_warehouse.extend_goods_to_store(cat, 0, *instances)

    # adding one good for testing
    store_warehouse.add_good_quantity('Топ цвета фуксии', 7)
    store_warehouse.add_good_quantity('Свитшот с капюшоном из футера “перья”', 3)

    return store_warehouse


def main():
    store_warehouse = OvalDanceWear()
    # print(store_warehouse.list_of_goods)
    print(store_warehouse.get_list_of_goods())
    # print(store_warehouse.get_goods_by_cat('Топы и свитшоты'))
    # print(store_warehouse.get_goods_and_quans())
    # print(store_warehouse.get_most_quantity_good())
    # print(store_warehouse.get_least_quantity_good())


if __name__ == '__main__':
    main()


